import credentials as c
import urequests as requests
import json

from time import sleep_ms, sleep, ticks_ms,  ticks_diff
import time
from machine import Pin, I2C, ADC
from ssd1306 import SSD1306_I2C

i2c = I2C(-1, Pin(4),Pin(5),freq=40000) # Bitbanged I2C bus
screen = SSD1306_I2C(128, 64, i2c)
screen.invert(1) # White text on black background


adc=ADC(Pin(36))
adc.atten(ADC.ATTN_0DB)
adc.width(ADC.WIDTH_12BIT)


HW_ID = 3
SERVER_URL = "https://em.dev.iota.pw/"
REQ_INTERVAL = 10000 # in ms
UPDATE_INTERVAL = 10000

wifi = None

btn = Pin(0, Pin.IN)


def connect_wifi(ssid=c.ssid, psk=c.psk):
    import network
    global wifi
    wifi = network.WLAN(network.STA_IF)
    wifi.active(True)
    wifi.connect(ssid, psk)
    while wifi.isconnected() == False:
        sleep(0.1)
        if btn.value() == 0:
            print("Not waiting fro Wifi...")
            break
    if wifi.isconnected():
        print("Connected: ",wifi.ifconfig()[0])


def read_co2():
    print("Reading CO2...")
    mnt = (adc.read()/4096) * 1.1
    for i in range(20):
        mnt = (mnt + (adc.read()/4096) * 1.15) / 2
        print(".", mnt)
        sleep(0.5)
    ppm = int( ((mnt-0.4)/1.6)*5000)
    if ppm < 350:
        ppm = 400
    return ppm


def http_request(method="GET", url=SERVER_URL, headers={}, data=None, json=None):
    try:
        if wifi == None:
            connect_wifi()
        req_status = None
        print("Sending {} request...".format(method))
        req = requests.request(method=method, url=url, headers=headers, data=data, json=json)
        req_status = [req.status_code, req.reason]
        req.close()
        if req_status is not None:
            print(req_status)
            return req
        else:
            return False
    except Exception as e:
        print("Exception at http_request: ", e)


#connect_wifi(c.ssid, c.psk)
#dht = read_dht()
#screen.text("Temperature: {}".format(dht[0]),0,0,0)
#screen.text("Humidity: {}".format(dht[1]),0,20,0)
#screen.show()

try:
    screen.fill(1)
    screen.text("Hello!",0,30,0)
    screen.show()
    last_print = time.ticks_ms()
    last_req = time.ticks_ms()
    connect_wifi(c.ssid, c.psk)
    screen.text("WiFi Con: {}".format(wifi.isconnected()),0,0,0)
    screen.show()
    while True:
        if btn.value() == 0:
            break
        cur_print = time.ticks_ms()
        cur_req = time.ticks_ms()
        
        if time.ticks_diff(cur_print, last_print) >= UPDATE_INTERVAL:
            last_print = cur_print
            print("Update sensor data...")
            mmt = read_co2()
            screen.fill(1)
            screen.text("{}".format(SERVER_URL.split('/')[2]),0,0,0)
            screen.text("{}".format(HW_ID), 120, 0,0)
            screen.text("_________________", 0, 4,0)
            screen.text("CO2: {} ppm".format(mmt),0,20,0)
            screen.show()
            print("Sensor data: {}".format(mmt))
        
        if time.ticks_diff(cur_req, last_req) >= REQ_INTERVAL:
            last_req = cur_req
            json_message = {"hw_id": HW_ID,"co2": mmt}
            #req_status = http_request(url="https://req.dev.iota.pw/", json=[{"temp": dht[0], "hum": dht[1]}])
            screen.text(".",0,50,0)
            screen.show()
            req_status = http_request(method="POST", url=SERVER_URL, json=json.dumps(json_message))
            if req_status != None:
                screen.text("{}".format(req_status.status_code),8,50,0)
            else:
                screen.text("None",0,50,0)
                print("Request returned 'None'")
            screen.show()
except Exception as e:
        print("Exception at main: ", e)




