import credentials as c
import gc
gc.enable()
import urequests as requests
import json
from time import sleep_ms, sleep, ticks_ms,  ticks_diff
from machine import Pin, I2C, ADC
from ssd1306 import SSD1306_I2C
import bme280_float
from dht import DHT22

from machine import freq
freq(80000000)


i2c = I2C(-1, Pin(4),Pin(5),freq=40000) # Bitbanged I2C bus
#i2c = I2C(0, I2C.MASTER, scl=Pin(4), sda=Pin(5), freq=400000) # Bitbanged I2C bus
screen = SSD1306_I2C(128, 64, i2c)
screen.invert(1) # White text on black background

bme=bme280_float.BME280(i2c=i2c)


HW_ID1 = 1
HW_ID2 = 2
HW_ID3 = 3
SERVER_URL = "https://em.dev.iota.pw/"
REQ_INTERVAL = 10000 # in ms
UPDATE_INTERVAL = 5000

wifi = None

btn = Pin(0, Pin.IN)
s_select = Pin(15, Pin.IN, Pin.PULL_UP)
d = DHT22(Pin(16))

adc=ADC(Pin(39))
adc.atten(ADC.ATTN_0DB)
adc.width(ADC.WIDTH_12BIT)




def connect_wifi(ssid=c.ssid, psk=c.psk):
    #import network
    from network import WLAN
    global wifi
    #wifi = network.WLAN(network.STA_IF)
    wifi = WLAN(0)
    wifi.active(True)
    wifi.connect(ssid, psk)
    while wifi.isconnected() == False:
        sleep(0.1)
        if btn.value() == 0:
            print("Not waiting fro Wifi...")
            break
    if wifi.isconnected():
        print("Connected: ",wifi.ifconfig()[0])


def read_bme():
  try:
    temp, hpa, hum = bme.values
    #if (isinstance(temp, float) and isinstance(hum, float)) or (isinstance(temp, int) and isinstance(hum, int)):
    #  msg = (b'{0:3.1f},{1:3.1f}'.format(temp, hum))
    #  hum = round(hum, 2)
    return temp, hum, hpa
    #else:
    #  return None #('Invalid sensor readings.')
  except OSError as e:
    print('Failed to read sensor: ', e)
    return None


def read_co2():
    print("Reading CO2...")
    mnt = (adc.read()/4096) * 1.1
    for i in range(20):
        mnt = (mnt + (adc.read()/4096) * 1.15) / 2
        print(".", mnt)
        sleep(0.2)
    ppm = int( ((mnt-0.4)/1.6)*5000)
    if ppm < 380:
        ppm = 380
    return ppm


def get_co2():
    ppm = 0
    av_mv = 0
    if adc.progress()[0] == False:
        av_mv = adc.collected()[2]
        adc.collect(1, len=10, readmv=True)
        ppm = ((av_mv-400)/1600)*5000
        if ppm < 350:
            ppm = 0
    return ppm, av_mv


def read_dht():
  try:
    d.measure()
    temp = d.temperature()
    hum = d.humidity()
    if (isinstance(temp, float) and isinstance(hum, float)) or (isinstance(temp, int) and isinstance(hum, int)):
      msg = (b'{0:3.1f},{1:3.1f}'.format(temp, hum))
      hum = round(hum, 2)
      return temp, hum
    else:
      return None #('Invalid sensor readings.')
  except OSError as e:
    print('Failed to read sensor: ', e)
    return None


def http_request(method="GET", url=SERVER_URL, headers={}, data=None, json=None):
    try:
        if wifi == None:
            connect_wifi()
        req_status = None
        print("Sending {} request...".format(method))
        req = requests.request(method=method, url=url, headers=headers, data=data, json=json)
        req_status = [req.status_code, req.reason]
        req.close()
        gc.collect()
        if req_status is not None:
            print(req_status)
            return req
        else:
            return False
    except Exception as e:
        print("Exception at http_request: ", e)



def draw_header():
    screen.fill(1)
    screen.text("{}".format(SERVER_URL.split('/')[2][:6]),0,0,0)
    screen.text("{}, {}, {}".format(HW_ID1, HW_ID2, HW_ID3), 70, 0,0)
    screen.text("_________________", 0, 4,0)
    screen.show()


#connect_wifi(c.ssid, c.psk)
#dht = read_dht()
#screen.text("Temperature: {}".format(dht[0]),0,0,0)
#screen.text("Humidity: {}".format(dht[1]),0,20,0)
#screen.show()

try:
    screen.fill(1)
    screen.text("Hello!",0,30,0)
    screen.show()
    last_print = ticks_ms()
    last_req = ticks_ms()
    connect_wifi(c.ssid, c.psk)
    screen.text("WiFi Con: {}".format(wifi.isconnected()),0,0,0)
    screen.show()
    sleep(1)
    draw_header()
    
    while True:
        if btn.value() == 0:
            break
        if not s_select.value():
            screen.contrast(10)
        else:
            screen.contrast(255)
        
        cur_print = ticks_ms()
        cur_req = ticks_ms()
        
        
        if ticks_diff(cur_print, last_print) >= UPDATE_INTERVAL:
            last_print = cur_print
            print("Update sensor data...")
            
            dht_ = read_dht()
            bme_ = read_bme()
            co2_= read_co2()
            
            draw_header()
            screen.text("1: T{} H{:.1f}".format(dht_[0], dht_[1]),0,20,0)
            screen.text("2: {:.1f}/{:.1f}/{:.1f}".format(bme_[0], bme_[1], bme_[2]),0,30,0)
            screen.text("3: {} ppm".format(co2_),0,40,0)       
            screen.show()
            
            print("Sensor data: 1: T{} H{:.1f}".format(dht_[0], dht_[1]))
            print("Sensor data: 2: {:.1f}/{:.1f}/{:.1f}".format(bme_[0], bme_[1], bme_[2]))
            print("Sensor data: 3: {} ppm".format(co2_))


        if ticks_diff(cur_req, last_req) >= REQ_INTERVAL:
            last_req = cur_req
            
            gc.collect()
            json_message = {"hw_id": HW_ID1,"temp": dht_[0], "hum": dht_[1]}
            screen.text(".",0,55,0)
            req_status = http_request(method="POST", url=SERVER_URL, json=json.dumps(json_message))
            if req_status != None:
                screen.text("{}".format(req_status.status_code),0,55,0)
            else:
                screen.text("None",0,55,0)
                print("Request returned 'None'")
            screen.show()
            
            gc.collect()
            json_message = {"hw_id": HW_ID2,"temp": bme_[0], "hum": bme_[1], "hPa": bme_[2]}
            screen.text(".",50,55,0)
            req_status = http_request(method="POST", url=SERVER_URL, json=json.dumps(json_message))
            if req_status != None:
                screen.text("{}".format(req_status.status_code),50,55,0)
            else:
                screen.text("None",50,55,0)
                print("Request returned 'None'")
            screen.show()    
            
            gc.collect()
            json_message = {"hw_id": HW_ID3,"co2": co2_}
            screen.text(".",90,55,0)
            screen.show()
            req_status = http_request(method="POST", url=SERVER_URL, json=json.dumps(json_message))
            if req_status != None:
                screen.text("{}".format(req_status.status_code),90,55,0)
            else:
                screen.text("None",90,55,0)
                print("Request returned 'None'")
            screen.show()
            
            gc.collect()
except Exception as e:
        print("Exception at main: ", e)





