import credentials as c
import urequests as requests
import json
import bme280_float
from time import sleep_ms, sleep, ticks_ms,  ticks_diff
import time
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
import bme280_float
i2c = I2C(-1, Pin(4),Pin(5),freq=40000) # Bitbanged I2C bus
screen = SSD1306_I2C(128, 64, i2c)
screen.invert(1) # White text on black background

i2c_bme = I2C(-1, Pin(15),Pin(13),freq=40000)
bme=bme280_float.BME280(i2c=i2c_bme)



HW_ID = 2
SERVER_URL = "https://em.dev.iota.pw/"
REQ_INTERVAL = 10000 # in ms
UPDATE_INTERVAL = 2000

wifi = None

btn = Pin(0, Pin.IN)


def connect_wifi(ssid=c.ssid, psk=c.psk):
    import network
    global wifi
    wifi = network.WLAN(network.STA_IF)
    wifi.active(True)
    wifi.connect(ssid, psk)
    while wifi.isconnected() == False:
        sleep(0.1)
        if btn.value() == 0:
            print("Not waiting fro Wifi...")
            break
    if wifi.isconnected():
        print("Connected: ",wifi.ifconfig()[0])


def read_bme():
  try:
    temp, hpa, hum = bme.values
    #if (isinstance(temp, float) and isinstance(hum, float)) or (isinstance(temp, int) and isinstance(hum, int)):
    #  msg = (b'{0:3.1f},{1:3.1f}'.format(temp, hum))
    #  hum = round(hum, 2)
    return temp, hum, hpa
    #else:
    #  return None #('Invalid sensor readings.')
  except OSError as e:
    print('Failed to read sensor: ', e)
    return None


def http_request(method="GET", url=SERVER_URL, headers={}, data=None, json=None):
    try:
        if wifi == None:
            connect_wifi()
        req_status = None
        print("Sending {} request...".format(method))
        req = requests.request(method=method, url=url, headers=headers, data=data, json=json)
        req_status = [req.status_code, req.reason]
        req.close()
        if req_status is not None:
            print(req_status)
            return req
        else:
            return False
    except Exception as e:
        print("Exception at http_request: ", e)


#connect_wifi(c.ssid, c.psk)
#dht = read_dht()
#screen.text("Temperature: {}".format(dht[0]),0,0,0)
#screen.text("Humidity: {}".format(dht[1]),0,20,0)
#screen.show()

try:
    screen.fill(1)
    screen.text("Hello!",0,30,0)
    screen.show()
    last_print = time.ticks_ms()
    last_req = time.ticks_ms()
    connect_wifi(c.ssid, c.psk)
    screen.text("WiFi Con: {}".format(wifi.isconnected()),0,0,0)
    screen.show()
    while True:
        if btn.value() == 0:
            break
        cur_print = time.ticks_ms()
        cur_req = time.ticks_ms()
        
        if time.ticks_diff(cur_print, last_print) >= UPDATE_INTERVAL:
            last_print = cur_print
            print("Wifi: {}, {}".format(wifi.isconnected(), wifi.ifconfig()[0]))
            print("Update sensor data...")
            mmt = read_bme()
            screen.fill(1)
            screen.text("{}".format(SERVER_URL.split('/')[2]),0,0,0)
            screen.text("{}".format(HW_ID), 120, 0,0)
            screen.text("_________________", 0, 4,0)
            screen.text("Temp: {:.2f} C".format(mmt[0]),0,20,0)
            screen.text("Hum: {:.2f} %".format(mmt[1]),0,30,0)
            screen.text("hPa: {:.2f} %".format(mmt[2]),0,40,0)
            screen.show()
            print("Sensor data: {}, {}, {}".format(mmt[0], mmt[1], mmt[2]))
        
        if time.ticks_diff(cur_req, last_req) >= REQ_INTERVAL:
            last_req = cur_req
            json_message = {"hw_id": HW_ID,"temp": mmt[0], "hum": mmt[1], "hPa": mmt[2]}
            #req_status = http_request(url="https://req.dev.iota.pw/", json=[{"temp": dht[0], "hum": dht[1]}])
            screen.text(".",0,50,0)
            screen.show()
            req_status = http_request(method="POST", url=SERVER_URL, json=json.dumps(json_message))
            if req_status != None:
                screen.text("{}".format(req_status.status_code),8,50,0)
            else:
                screen.text("None",0,50,0)
                print("Request returned 'None'")
            screen.show()
except Exception as e:
        print("Exception at main: ", e)




