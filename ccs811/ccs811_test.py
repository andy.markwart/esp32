import time
import adafruit_ccs811
from adafruit_ccs811 import CCS811

from machine import Pin, I2C

# construct an I2C bus
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=100000)

#ccs811 = adafruit_ccs811.CCS811(i2c)
ccs811 = CCS811(i2c)

# Wait for the sensor to be ready and calibrate the thermistor
while not ccs811.data_ready:
    pass
temp = ccs811.temperature
ccs811.temp_offset = temp - 25.0

while True:
    print("CO2: {} PPM, TVOC: {} PPM, Temp: {} C"
          .format(ccs811.eco2, ccs811.tvoc, ccs811.temperature))
    time.sleep(0.5)