import credentials as c
from machine import freq, Pin, deepsleep
from time import sleep, sleep_ms, ticks_ms
import esp32, machine, network


wifi = None


btn0 = Pin(0, Pin.IN, Pin.PULL_UP)


wifi_conf = ['192.168.101.70', '255.255.255.0', '192.168.101.1', '1.1.1.1']
#wifi_conf = ['192.168.43.70', '255.255.255.0', '192.168.43.1', '1.1.1.1']

if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
else:
    print("reset_cause:", machine.reset_cause())



def connect_wifi(ssid=c.ssid, psk=c.psk):
    global wifi
    wifi = network.WLAN(network.STA_IF)
    wifi = network.WLAN(0)
    wifi.ifconfig(wifi_conf)
    wifi.active(True)
    wifi.connect(ssid, psk)
    #wifi.connect(ssid)
    while wifi.isconnected() == False:
        sleep_ms(10)
    if wifi.isconnected():
        # Change DNS server to cloudflare as UNitymedia doesn't work
        #y = list(wifi.ifconfig())
        #y[3]='1.1.1.1'
        #wifi.ifconfig(y)
        print("Connected: ",wifi.ifconfig())



if btn0.value() == 0:
    print("Button 0 low, exiting...")
else:
    start = ticks_ms()
    connect_wifi()
    end = ticks_ms()
    print("Elapsed time {}ms".format(end-start))

    print("going to sleep")
    esp32.wake_on_ext0(pin = btn0, level = 0)
    deepsleep()

