## Pinmap
![ESP32 WROOVER Pinmap](/media/esp32_wroover_pinmap.png)

## Functions
### Deep Sleep
[Insight Into ESP32 Sleep Modes & Their Power Consumption](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-hibernation-mode)


## Download MicroPython binaries

[All MicroPython binaries](https://micropython.org/download#esp32)

[esp32-20190529-v1.11.bin](https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin)

[Flash MicroPython](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html)


```bash
sudo adduser <USERNAME> dialout
sudo login -f <USERNAME>
pip install esptool
wget -O esp32-20190529-v1.11.bin https://micropython.org/resources/firmware/esp32-20190529-v1.11.bin
esptool.py --port /dev/ttyUSB0 erase_flash
esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20190529-v1.11.bin
sudo screen /dev/ttyUSB0 115200
```


## IDE 
[Thonny IDE](https://randomnerdtutorials.com/getting-started-thonny-micropython-python-ide-esp32-esp8266/)

```bash
sudo apt install python3 python3-pip python3-tk
sudo pip3 install thonny
```




## Boards

![](https://i1.wp.com/randomnerdtutorials.com/wp-content/uploads/2018/08/ESP32-DOIT-DEVKIT-V1-Board-Pinout-36-GPIOs-updated.jpg?w=750&ssl=1)
### WEMOS LOLIN32 
[Banggood.com](https://www.banggood.com/Wemos-ESP32-OLED-Module-For-Arduino-ESP32-OLED-WiFi-Bluetooth-Dual-ESP-32-ESP-32S-ESP8266-p-1148119.html) 

Initialize OLED with pins 5 (SDA) and 4 (SCK): 
```
SSD1306Wire  display(0x3c, 5, 4);
``` 

### Wemos TTGO LORA32 
[Banggood.com](https://www.banggood.com/2Pcs-Wemos-TTGO-LORA32-868915Mhz-ESP32-LoRa-OLED-0_96-Inch-Blue-Display-p-1239769.html) 

[YouTube](https://www.youtube.com/watch?v=WHl2oC8fqZU)

### Wemos TTGO OLED 

[Banggood.com](https://www.banggood.com/16-Mt-Bytes-128-Mt-bit-Pro-ESP32-OLED-V2_0-TTGO-For-Arduino-ESP32-OLED-WiFi-ModulesBluetooth-p-1205876.html) 
From Banggood comment: 
```
This board works great, I was able to get both the WiFi and the OLED up and running. The OLED is a bit tricky, it's not connected to 
the standard I2C pins, its SDA pin is 4 and SCL pin is 15. You can set this up by adding Wire.begin(4, 15) to your setup code. 
The display's I2C address is 0x3C. You will also need to use the OLED_RST pin to enable the display: 
pinMode(16,OUTPUT); digitalWrite(16, LOW); delay(50); digitalWrite(16, HIGH); 
After applying these settings I2CScan can find the display and most SSD1306 Arduino libraries work 
(for example the Adafruit SSD1306 works after changing Wire.begin() to Wire.begin(4, 15) and setting the reset pin and the I2C address)
```

# Libraries
## OLED
[128x64 OLED (SSD1306)](https://github.com/ThingPulse/esp8266-oled-ssd1306) 


