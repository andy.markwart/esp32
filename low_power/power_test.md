###ESP8266 12E bare
[Code](https://randomnerdtutorials.com/micropython-esp8266-deep-sleep-wake-up-sources/)
 - VCC: 3 V by 2 AA mignon cells
 - sleep current:
    - 0.016 mA = 16 µA (machine.deepsleep())

### ESP32 WROOM-32 bare
 - VCC: ~3 V by one LiFePo4 cell
 - sleep current:
    - 0.006 mA = 6 µA (machine.deepsleep()) 
[ESP32 Pinout Infos](http://acoptex.com/project/1226/basics-project-072r-esp32-development-board-gpio-pins-guide-at-acoptexcom)



###ESP32 CAM board
 - cam disabled
 - VCC: 3 V by 2 AA mignon cells
 - sleep current: 
    - 2,776 mA (machine.sleep())
    - 1,777 mA (machine.deepsleep())
 - wake: 7,2 mA