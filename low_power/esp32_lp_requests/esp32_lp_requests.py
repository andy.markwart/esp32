"""
ESP32 low power

"""
import machine
from machine import freq
from machine import Pin
from machine import deepsleep
from machine import RTC
from machine import ADC
from time import sleep
import sys
import esp32
try:
  import usocket as socket
except:
  import socket
try:
  import urequests as requests
except:
  import requests
import network
import esp
esp.osdebug(None)       # turn off vendor O/S debugging messages

#if not machine.reset_cause() == machine.DEEPSLEEP_RESET:
rtc = RTC()
#rtc.datetime((2019, 7, 28, 22, 10, 48, 0, 0))

print("\n")
# set processor frequency from 240 MHz to 80MHz
machine.freq(80000000)
res = Pin(0, Pin.IN)
led = Pin(13, Pin.OUT)
wake1 = Pin(15, Pin.IN)
print("")


def get_bat_voltage():
    adc = ADC(Pin(35))
    adc.atten(ADC.ATTN_11DB)          # 0 - 3.6 V
    volt = (adc.read() / 4095) * 3.6 * 2.0 # raw Voltage *2.0 for voltage devider
    #print("get_bat_voltage()", volt)
    return volt


def rtc_insert(data, pos=None):
    global rtc
    rtc_m = rtc.memory().decode().split(',')
    if pos == None:
        rtc_m.append(str(data))
    else:
        rtc_m[pos] = str(data)
    rtc_m_new = ""
    for entry in rtc_m:
        rtc_m_new += str(entry) + ","
    rtc_m_new = rtc_m_new[:-1]
    rtc.memory(rtc_m_new)



def rtc_del(pos):
    global rtc
    rtc_m = rtc.memory().decode().split(',')
    #if len(rtc_m) > pos:
    #    rtc_m.pop(pos)
    rtc_m[pos] = ""
    rtc_m_new = ""
    for entry in rtc_m:
        rtc_m_new += str(entry) + ","
    rtc_m_new = rtc_m_new[:-1]
    rtc.memory(rtc_m_new)
    
def rtc_read(pos=None):
    global rtc
    rtc_m = rtc.memory().decode().split(',')
    if pos == None:
        return rtc_m
    else:
        return rtc_m[pos]


# gets only called once after a reset (not after deepsleep reset)
def init():
    global rtc
    rtc.memory('0,,,,,,,,,,') # fill rtc memory with empyt indexes
    #print("BAT_V:", get_bat_voltage())

if len(rtc.memory()) == 0: # first system start after reset
    print("INIT, rtc_memory:", rtc.memory())
    init()

while True:
    #try:
    if rtc_read(10) == "":
        led.on()
        print("LED on")
        #print("TEMPERATURE:",((esp32.raw_temperature()-32)*5)/9 )
        
        # exit main (for programming)
        if res.value() == 0:
            print("GPIO 0 is low, exiting...")
            sys.exit()
       
        #if int(rtc_read(0)) % 5 == 0:
        rtc_insert(get_bat_voltage(), 1)
        rtc_insert('adc',10)

        print(">>>>>>>>>> rtc.memory:", rtc.memory())
        
        rtc_m = rtc.memory().decode().split(',')
        print("rtc_m[0]", rtc_m[0])
        rtc_counter = int(rtc_m[0]) + 1
        rtc_m[0] = str(rtc_counter)
        rtc_m_new = ""
        for entry in rtc_m:
            rtc_m_new += str(entry) + ","
        rtc_m_new = rtc_m_new[:-1]
        rtc.memory(rtc_m_new)
        #except Exception as e:
        #    print("ERROR:   rtc.memory:", e)
        
        #print(rtc.datetime())
        if machine.reset_cause() == machine.DEEPSLEEP_RESET:
            print('reset_cause: deepsleep')
        else:
            print("reset_cause:", machine.reset_cause())
        
        #print("BAT_V:", get_bat_voltage())
        #machine.sleep(1000)
        print("LED off")
        led.off()
        #deepsleep()
        
    if rtc_read(10) == "adc":
        rtc_insert("adcd", 10)
        print("\n*** ADC deepsleep ***\n")
        deepsleep(1)
    elif rtc_read(10) == "adcd":
        rtc_del(10)
        esp32.wake_on_ext0(pin = wake1, level = esp32.WAKEUP_ALL_LOW)
        print("\n*** Going to DEEPSLEEP ***\n")
        deepsleep()




