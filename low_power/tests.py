"""
ESP32 low power

"""
import machine
from machine import freq, Pin, deepsleep
from machine import sleep as standby
from time import sleep
import sys
import esp32


# set processor frequency from 240 MHz to 80MHz
freq(80000000)
led = Pin(5, Pin.OUT)
wake1 = Pin(0, Pin.IN, Pin.PULL_UP)


if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
else:
    print("reset_cause:", machine.reset_cause())



while True:
    # exit main (for programming)
    sleep(1)
    if wake1.value() == 0:
        print("GPIO 0 is low, exiting...")
        sys.exit()
        
    print("led on")
    led.on()
    sleep(1)
    led.off()
    sleep(.3)
    
    print("going to sleep")
    esp32.wake_on_ext0(pin = wake1, level = 0)
    #standby()
    deepsleep()
    print("DIRECT WAKEUP")



