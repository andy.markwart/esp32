"""
ESP8266 low power

"""
import esp
esp.osdebug(None)       # turn off vendor O/S debugging messages

import machine
from machine import freq
from machine import Pin
from machine import deepsleep
from time import sleep
import sys


#rtc = machine.RTC()
#rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
# set RTC.ALARM0 to fire after 10 seconds (waking the device)
#rtc.alarm(rtc.ALARM0, 10000)

led = Pin(2, Pin.OUT)
stop = Pin(5, Pin.IN, Pin.PULL_UP)


print("cpu:",machine.freq())
print(stop.value())
if stop.value() == 0:
    print("GPIO 5 is low, exiting...")
    sys.exit()
else:
    print("inside while")
    #print(rtc.datetime())
    # exit main (for programming)
        
    print("led on")
    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('woke from a deep sleep')
    else:
        print("reset_cause:", machine.reset_cause())
    led.on()
    sleep(1)
    led.off()
    sleep(.3)
    print("going to sleep")
    
    
    #machine.sleep(10000)
    deepsleep()
    print("DIRECT WAKEUP")




