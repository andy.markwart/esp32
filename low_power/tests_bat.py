"""
ESP32 low power

"""
import machine
from machine import freq
from machine import Pin
from machine import deepsleep
from machine import RTC
from machine import ADC
from time import sleep
import sys
import esp32



#if not machine.reset_cause() == machine.DEEPSLEEP_RESET:
rtc = RTC()
rtc.datetime((2019, 7, 28, 22, 10, 48, 0, 0))

print("\n")
# set processor frequency from 240 MHz to 20MHz
print("cpu:",machine.freq())
machine.freq(20000000)
res = Pin(0, Pin.IN)
led2 = Pin(2, Pin.OUT)
led = Pin(13, Pin.OUT)
wake1 = Pin(15, Pin.IN)
#print("outsid while")
print("cpu:",machine.freq())
print("")


def get_bat_voltage():
    adc = ADC(Pin(33))
    adc.atten(ADC.ATTN_11DB)          # 0 - 3.6 V
    volt = (adc.read() / 4095) * 3.6  # raw Voltage
    adc = None
    return volt * 2.0                 # voltage devider


while True:
    # exit main (for programming)
    if res.value() == 0:
        print("GPIO 0 is low, exiting...")
        sys.exit()
    
    led.on()
    print("LED on")
    
    print(">>>>>>>>>> rtc.memory:", rtc.memory())
    if len(rtc.memory()) == 0: # first system start
        rtc.memory('0')
    #print(rtc.datetime())
    if machine.reset_cause() == machine.DEEPSLEEP_RESET:
        print('reset_cause: deepsleep')
    else:
        print("reset_cause:", machine.reset_cause())
        
    print("BAT_V:", get_bat_voltage())
    try:
        rtc_m = int(rtc.memory())
        rtc.memory(str(rtc_m + 1))
    except Exception as e:
        print("rtc.memory:", e)
    print("going to deepsleep")
    esp32.wake_on_ext0(pin = wake1, level = esp32.WAKEUP_ALL_LOW)
    
    print("LED off")
    led.off()
    
    deepsleep()
    #machine.sleep()
    print("DIRECT WAKEUP")



