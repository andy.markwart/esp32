from uQR import QRCode


from time import sleep_ms, sleep
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
i2c = I2C(-1, Pin(4),Pin(5),freq=40000) # Bitbanged I2C bus
assert 60 in i2c.scan(), "No OLED display detected!"

screen = SSD1306_I2C(128, 64, i2c)
qr = QRCode(border=2)






#qr.add_data('IKKGRLEALLLLOCOW9BIHCACIJPVOJILQMEHJJFJABNAOLDQKXEL9PKCAGOYL9JGYHCPFAPZPS9VWXYTGBRYTXHKMDX')
qr.add_data('IKKGRLEALLLLOCOW9BIHCACIJPVOJIL')
matrix = qr.get_matrix()

screen.poweron()
screen.contrast(255)
scale = 2  #set to 1 or 2
for y in range(len(matrix)*scale):                   # Scaling the bitmap by 2
    for x in range(len(matrix[0])*scale):            # because my screen is tiny.
        value = not matrix[int(y/scale)][int(x/scale)]   # Inverting the values because
        screen.pixel(x, y, value)                # black is `True` in the matrix.
screen.show()                                    