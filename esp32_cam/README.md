# esp32_cam

[Geekcreit ESP32-Cam](https://www.banggood.com/Geekcreit-ESP32-CAM-WiFi-bluetooth-Camera-Module-Development-Board-ESP32-With-Camera-Module-OV2640-p-1394679.html?cur_warehouse=CN)
OV2640


[TTGO-Cam aka LilyGO](https://www.banggood.com/TTGO-T-Journal-ESP32-Camera-Development-Board-OV2640-SMA-WiFi-3dbi-Antenna-0_91-OLED-Camera-Board-p-1379925.html)


[Github: esp32-camera-series](https://github.com/lewisxhe/esp32-camera-series)

[ESP32-CAM Video Streaming and Face Recognition with Arduino IDE](https://randomnerdtutorials.com/esp32-cam-video-streaming-face-recognition-arduino-ide/)


[#193 Comparison of 10 ESP32 Battery powered Boards without display (incl. deep-sleep)](https://www.youtube.com/watch?v=-769_YIeGmI)
[#182 ESP32 Lora Boards: What you need to know before you buy (incl. Antenna knowledge)](https://www.youtube.com/watch?v=CJNq2I_PDHQ)

